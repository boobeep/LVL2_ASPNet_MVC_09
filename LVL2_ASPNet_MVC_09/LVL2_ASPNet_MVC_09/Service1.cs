﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LVL2_ASPNet_MVC_09
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmpExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                /*ThreadStart start = new ThreadStart(Working);
                Worker = new Thread(start);
                Worker.Start();*/
                tmpExecutor.Elapsed += new System.Timers.ElapsedEventHandler(Working);
                tmpExecutor.Interval = 10000;   //Set time
                tmpExecutor.Enabled = true;
                tmpExecutor.Start();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Working(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Do your process here
            string path = "C:\\imam.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format("Windows Service is called on " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ""));
                writer.Close();
            }
            Thread.Sleep(ScheduleTime * 10 * 1000);
        }

        protected override void OnStop()
        {
            try
            {
                tmpExecutor.Enabled = false;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
